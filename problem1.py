def fun(str, n):
    c = [0] * (n+1) 
    c[0] = 1
    c[1] = 1
    for i in range(2, n+1):
        c[i] = 0
        if (str[i-1] > '0'):
            c[i] = c[i-1]
        if (str[i-2] == '1' or (str[i-2] == '2' and str[i-1] < '7') ):
            c[i] += c[i-2]
    return c[n]

#str=input()	
str="111"
print("Number of ways to decode: ",fun(str, len(str)))
