var json=[
{
	id:"2",
	tags:[
	{"key":"Name",
	"value":"Tag2"
	},
	{"key":"Role",
	"value":"Publisher"
	},
		{"key":"Foo",
	"value":"Bar"
	}]
},
{
	id:"1",
	tags:[
	{"key":"Name",
	"value":"Tag1"
	},
	{"key":"Role",
	"value":"Subscriber"
	},
		{"key":"Tao",
	"value":"Ching"
	}]
}
];

json.sort(function (a, b) {
    return a.id.localeCompare(b.id);
});
console.log("ID, NAME, ROLE");
json.forEach(k =>{
	var name;
	var role;
	k.tags.forEach(i =>{
		if(i.key==="Name")
			name=i.value;
		if(i.key==="Role")
			role=i.value;
	});
	console.log(k.id+", "+name+", "+role);
});

